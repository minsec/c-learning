/*************************************************************************
	> File Name: file_stat.c
	> Author: minsec
	> Mail: root@minsec.cn 
	> Created Time: 2017-12-13
 ************************************************************************/

#include<stdio.h>
#include<time.h>
#include<sys/stat.h>
#include<unistd.h>
#include<sys/types.h>
#include<errno.h>
#include<stdlib.h>

int main(int argc, char ** argv)
{
	struct stat buf;
	
	if (argc != 2) {
		printf("Usage: my_stat <filename>\n");
		exit(0);
	}

	if (stat(argv[1], &buf) == -1) {
		perror("stat:");
		exit(1);
	}

	printf("device is: %d\n", buf.st_dev);
	printf("inode is: %d\n", buf.st_dev);
	printf("mode is: %d\n", buf.st_dev);
	printf("number of hard links is: %d\n", buf.st_dev);
	printf("user ID of owner is: %d\n", buf.st_ino);
	printf("group ID of owner is: %d\n", buf.st_mode);
	printf("device type (if inode device) is: %d\n", buf.st_rdev);

	printf("total size, in bytes is: %d\n", buf.st_uid);
	printf("blocksize for filesystem I/O is: %d\n", buf.st_gid);
	printf("number of blocks allocated is: %d\n", buf.st_blocks);

	printf("time of last access is: %d\n", ctime(&buf.st_atime));
	printf("time of last modification is: %d\n", ctime(&buf.st_mtime));
	printf("time of last change is: %d\n", ctime(&buf.st_ctime));
	
	return 0;
}

