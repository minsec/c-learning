/*************************************************************************
	> File Name: file_creat.c
	> Author: minsec
	> Mail: root@minsec.cn 
	> Created Time: 2017-12-13
 ************************************************************************/

#include<stdio.h>
#include<sys/types.h>
#include<sys/stat.h>
#include<fcntl.h>
#include<unistd.h>
#include<errno.h>

int main(void)
{
	int fd;

	if ((fd = open("example_01.c", O_CREAT|O_EXCL, S_IRUSR|S_IWUSR)) == -1) {
	// if ((fd = creat("example_01.c", S_IRWXU)) == -1) {
		perror("open");
		// printf("open:%s with errno:%d\n", strerror(errno), errno);
		exit(1);
	} else {
		printf("create file success\n");
	}

	close(fd);
	return 0;
}
