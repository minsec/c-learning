/*************************************************************************
	> File Name: string_puts_gets.c
	> Author: minsec
	> Mail: root@minsec.cn 
	> Created Time: 2017-12-13
 ************************************************************************/

#include<stdio.h>

int main(void)
{
	char a[] = "Welcome to ";
	char *p = "Linux C Program";

	char string[20];

	gets(string);

	puts(string);

	puts(a);
	puts(p);

	return 0;
}
