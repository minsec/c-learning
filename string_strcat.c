/*************************************************************************
	> File Name: string_strcat.c
	> Author: minsec
	> Mail: root@minsec.cn 
	> Created Time: 2017-12-13
 ************************************************************************/

#include<stdio.h>
#include<string.h>

int main(void)
{
	char d[20] = "Hello ";
	char *s = "World";

	strcat(d,s);

	return 0;
}

