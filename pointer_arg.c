/*************************************************************************
	> File Name: pointer_arg.c
	> Author: minsec
	> Mail: root@minsec.cn 
	> Created Time: 2017年12月12日
 ************************************************************************/

#include<stdio.h>

int get_big(int i, int j)
{
	return i>j?i:j;
}

int get_max(int i, int j, int k, int (*p)(int, int))
{
	int ret;

	ret = p(i, j);
	ret = p(ret, k);

	return ret;
}

int main(void)
{
	int i = 5,j = 10,k = 15, ret;

	ret = get_max(i,j,k,get_big);
	printf("The MAX is %d\n", ret);

	return 0;
}
