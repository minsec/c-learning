/*************************************************************************
	> File Name: uid_demo.c
	> Author: minsec
	> Mail: root@minsec.cn 
	> Created Time: 2017-12-14
 ************************************************************************/

#include<stdio.h>
#include<unistd.h>
#include<fcntl.h>
#include<errno.h>
#include<stdlib.h>

extern int errno;

int main(void)
{
	int fd;

	printf("uid study:\n");
	printf("Process's uid = %d, euid = %d\n", getuid(), geteuid());

	if ((fd = open("test.dat", O_RDWR)) == -1) {
		printf("Open failure, errno is %d :%s \n", errno, strerror(errno));
		exit(1);
	} else {
		printf("Open successfully!\n");
	}

	close(fd);
	exit(0);
}
