/*************************************************************************
	> File Name: string_strcpy.c
	> Author: minsec
	> Mail: root@minsec.cn 
	> Created Time: 2017-12-13
 ************************************************************************/

#include<stdio.h>
#include<string.h>

// char *strcpy(char *dest, char *src);
// char *strncpy(char *dest, char *src, int n);

int main(void)
{
	char *s = "Hello World";
	char d1[20], d2[20];

	strcpy(d1, s);
	strcpy(d2, s, sizeof(s));

	return 0;
}
