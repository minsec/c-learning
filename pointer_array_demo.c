/*************************************************************************
	> File Name: pointer_array_demo.c
	> Author: minsec
	> Mail: root@minsec.cn 
	> Created Time: 2017年12月12日
 ************************************************************************/

#include<stdio.h>

int main(void)
{
	/*
	int a[] = {2,4,6,8,10};
	int *p1 = a;
	int *p2 = &a[0];

	p1 = p1 + 3;	// a[2]

	p1 = a + 5;		// a[4]
	*/

	int a[10], i, *p = NULL;

	for (i=0; i<10; i++)
		a[i] = i;

	for (i=0; i<10; i++)
		printf("%d ", a[i]);
	printf("\n");

	for (i=0; i<10; i++)
		printf("%d ", *(a+i));
	printf("\n");

	for (p=a; p<a+10; )
		printf("%d ", *p++);	// same priority, <-, eq *(p++)
	printf("\n");

	return 0;
}
// *p++ a[i++]
// *p-- a[i--]
// *++p a[++i]
// *--p a[--i]

