/*************************************************************************
	> File Name: pointer_return.c
	> Author: minsec
	> Mail: root@minsec.cn 
	> Created Time: 2017年12月12日
 ************************************************************************/

#include<stdio.h>

char *name[7] = {"Monday", "Tuesday", "Wednesday", "Thuesday", "Friday", "Saturday", "Sunday"};

char *message = "wrong input";

char *week(int day)
{
	if (day<0 || day>7)
		return message;
	else
		return name[day-1];
}

int main(void)
{
	int day;
	char *p;

	printf("Input a number of a week:\n");
	scanf("%d", &day);

	p = week(day);
	printf("%s\n", p);

	return 0;
}
