/*************************************************************************
	> File Name: putchar_demo.c
	> Author: minsec
	> Mail: root@minsec.cn 
	> Created Time: 2017年12月11日
 ************************************************************************/

#include<stdio.h>

int main()
{
	char a;

	a = getchar();

	putchar(a);
	putchar('\n');

	return 0;
}
