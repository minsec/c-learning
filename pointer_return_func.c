/*************************************************************************
	> File Name: pointer_return_func.c
	> Author: minsec
	> Mail: root@minsec.cn 
	> Created Time: 2017年12月12日
 ************************************************************************/

#include<stdio.h>

int get_big(int i,int j)
{
	return i>j?i:j;
}

int (*get_function(int a))(int, int)	// return func pointer func
{
	printf("the number is %d\n", a);

	return get_big;
}

int main(void)
{
	int i = 5,j = 10,max;

	int (*p)(int, int);
	p = get_function(100);

	max = p(i, j);

	printf("The MAX is %d\n", max);

	return 0;
}
// get_function(int a) -> int(*)(int, int)
// get_big -> function entry addr, pointer
